<?php

use App\Student;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/profile', 'HomeController@profile')->name('profile')->middleware('auth');

Route::post('/profile-save', 'HomeController@profile_save')->name('profile-save')->middleware('auth');

Route::get('/users/all', 'HomeController@users_all')->name('users-all')->middleware('auth');

Route::get('/users/', 'HomeController@users_all')->name('users-all')->middleware('auth');

Route::get('/users-register', 'UserRegisterController@index')->name('users-register');

Route::post('/users-register', 'UserRegisterController@post_register_user')->name('post-users-register');

Route::get('/users/add', 'HomeController@users_add')->name('users-add')->middleware('auth');

Route::post('/users/save', 'HomeController@users_save')->name('users-save')->middleware('auth');

Route::get('/user/qr/{id}/{download?}', 'HomeController@process_qr')->name('process-qr')->middleware('auth');

Route::get('/user/generate-id-card/{id}/{download?}', 'HomeController@generate_id_card')->name('generate-id-card')->middleware('auth');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');