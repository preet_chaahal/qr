@extends('themes.lumino.layouts.master')

@section('extraStyles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb hidden">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">Icons</li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Liste utilisateurs</h3>
		</div>
	</div><!--/.row-->
							
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				
				<!-- <div class="panel-heading">Liste utilisateurs</div> -->
				<div class="panel-body">
					<table id="userDataTbl" class="table table-striped table-bordered" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				            	<th></th>
				                <th>Nom utilisateur</th>
				                <th>Prenom</th>
				                <th>Nom de famille</th>
				                <th>Date de Nais.</th>
				                <th>Phone</th>
				                <th>Adresse</th>
				                <th>Actions</th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				            	<th></th>
				                <th>Nom utilisateur</th>
				                <th>Prenom</th>
				                <th>Nom de famille</th>
				                <th>Date de Nais.</th>
				                <th>Phone</th>
				                <th>Adresse</th>
				                <th>Actions</th>
				            </tr>
				        </tfoot>
				        <tbody>
				            @foreach ($students as $student)
					            <tr>
					            	<td><img style="height: 100px; width: 90px;" 
					            			<?php $image_url = url('images/students').'/'.$student->id_url; 
									if (! @getimagesize($image_url))
										$image_url = $_SERVER['HTTP_HOST'].'/images/uploads/students/'.$student->id_url; 
									?>
									@if($student->id_url != '')
					            				src = "{{ $image_url }}"
										src_old="{{ url('images/students') }}/{{ $student->id_url }}"
					            			@else
					            				src="{{ url('images/students') }}/no_user_thumb.png"
					            			@endif
					            		></td>
					                <td>{{ $student->username }}</td>
					                <td>{{ $student->fname or 'N.A.' }}</td>
					                <td>{{ $student->lname or 'N.A.' }}</td>
					                <td><?php $dob = \DateTime::createFromFormat('Y-m-d', $student->dob); echo $dob->format('d/m/Y'); ?></td>
					                <td>{{ $student->phone or 'N.A.' }}</td>
					                <td>{{ $student->address or 'N.A.' }}</td>
					                <td>
				                		<a href="{{ route('process-qr', ['id' => $student->id]) }}" class="btn btn-success btn-xs">Download QR Code</a>
					                	<a href="{{ route('process-qr', ['id' => $student->id, 'download' => 0]) }}" class="btn btn-info btn-xs">View QR Code</a>
					                	<div style="margin: 2px 0;"></div>
					                	<a href="{{ route('generate-id-card', ['id' => $student->id]) }}" class="btn btn-warning btn-xs">Download ID Card</a>
					                	<a href="{{ route('generate-id-card', ['id' => $student->id, 'download' => 0]) }}" class="btn btn-danger btn-xs">View ID Card</a>
					                </td>
					            </tr>
				            @endforeach
				        </tbody>
				    </table>
				</div><!-- /.panel-body -->
			
			</div>
		</div><!--/.col-->
	</div><!--/.row-->
</div>	<!--/.main-->
@endsection

@section('extraScripts')
<!-- Data Table -->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">	
	$(document).ready(function() {
	    $('#userDataTbl').DataTable( {
	    	"language": {
	            "lengthMenu": "Lister _MENU_ donnees",
	            "zeroRecords": "Nothing found - sorry",
	            "info": "Lister page _PAGE_ of _PAGES_",
	            "infoEmpty": "No records available",
	            "infoFiltered": "(filtered from _MAX_ total records)",
	        	"search": "Chercher:"
	        }	
    	});
	} );
</script>
@endsection
