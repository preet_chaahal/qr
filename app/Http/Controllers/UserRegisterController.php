<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Student;
use App\School;
use App\User;
use Validator;

/* For generating QR Code */
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
/* /For QR Code */

use App\Http\Controllers\MiscController;
use App\Http\Controllers\HomeController;
/* Intervention Image */
use Intervention\Image\ImageManagerStatic as Image;

class UserRegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = new MiscController;
        $countries = json_decode($countries->fetch_countries_data());

        $schools = School::where('removed', 0)
                                    ->get();

        $data = [
            'countries' => $countries,
            'schools'   => $schools,
            'page'      => 'users-all'
        ];
        
        return view('themes.lumino.pages.users-register', $data);
    }

    public function users_register()
    {
        $countries = new MiscController;
        $countries = json_decode($countries->fetch_countries_data());

        $schools = School::where('removed', 0)
                                    ->get();

        $data = [
            'countries' => $countries,
            'schools'   => $schools,
            'page'      => 'users-all'
        ];
        return view('themes.lumino.pages.users-register', $data);
    }

    public function post_register_user(Request $request)
    {
        $this->validate($request, [
            'id_url' => 'mimes:jpeg,jpg,png',
            'email'  => 'required|unique:students',
            'enrolment_id' => 'required|unique:students,enrolment_id|max:255'
        ]);
        // echo $request->input('dob');
        // die();
        $status = 0;
        $message = "Something went wrong trying to save the record";
        
        $form_data = $request->all();
        // print_r($form_data);
        $form_data['name'] = $request->fname ." ". $request->lname;
        $form_data['password'] = Hash::make($request->enrolment_id);
        $form_data['email'] = $request->email;

        $modified_image_name = '';
        if($request->hasFile('id_url')){
            $image = $request->file('id_url');
            $modified_image_name = 'up_'.time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('images/students'), $modified_image_name);
            
            //Resizing the image
            $path = public_path("images/students/".$modified_image_name);
            Image::make($path)->resize(168, 180)->save();
            
    	    // Saving to other location as well
    	    try{
                copy($path, '/var/www/html/images/uploads/students/'.$modified_image_name);
            } catch (\Exception $e) {
                // Do nothing
            }
    	}

        $form_data['id_url'] = $modified_image_name;

        /* Require discussion */
        $form_data['added_by'] = '63';
        // $form_data['enrolment_id'] = 'KHDTEY213547HD';
        $form_data['qr_id'] = '';
        /* //Require discussion */

        // print_r($form_data);
        // die();
        $student = Student::create($form_data);
        if( $student ){
            $data = [
                'status'    =>  1,
                'message'   =>  'Registeration successful!'
            ];
        }
        
        $HomeControllerObj = new HomeController;
        $HomeControllerObj->generate_id_card($student->id, 0);
        // EBign a redirect request sesison flash doesnt retian the session data
        // $request->session()->flash('status', $data);
        return redirect()->route('login', $data);
    }

}