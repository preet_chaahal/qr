@extends('layouts.app')

@section('extraStyles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Enrolement</div>

                <div class="panel-body">

				<form onsubmit="checkPasswords()" role="form" method="post" action="{{ route('post-users-register') }}" enctype="multipart/form-data">
					<fieldset>
						{{ csrf_field() }}
						<div class="form-group">
							<label>Prenom</label>
							<input required class="form-control" placeholder="Prenom" eng="First name" name="fname" type="text" autofocus="" maxlength="50">
						</div>
						
						<div class="form-group">
							<label>Nom de famille</label>
							<input required class="form-control" placeholder="Nom de famille" eng="Last name" name="lname" type="text" autofocus="" maxlength="50">
						</div>
						
						<div class="form-group">
							<label>Email</label>
							<input required class="form-control" placeholder="Email" eng-name="Email" name="email" type="email" value="" minlength="3" maxlength="255">
						</div>

						<div class="form-group">
							<label>Nom utilisateur</label>
							<input required class="form-control" placeholder="nom utilisateur" eng-name="Username" name="username" type="text" value="" minlength="3" maxlength="50">
						</div>

						<div class="form-group">
							<label>Date de Nais</label>
							<input required class="form-control datepicker" placeholder="DOB" name="dob" type="text" value="">
						</div>
						
						<!-- <div class="form-group">
							<input class="form-control" placeholder="POB" name="pob" type="text" value="">
						</div> -->
						
						<div class="form-group">
							<label>Sexe</label>
							<div class="radio">
								<label>
									<input type="radio" name="sex" id="optionsRadios1" value="m" checked>Homme
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="sex" id="optionsRadios2" value="f">Femme
								</label>
							</div>
						</div><!-- //form-group -->

						<div class="form-group">
							<label>Statut Marital</label>
							<div class="radio">
								<label>
									<input type="radio" name="mstatus" id="optionsRadios1" value="s" checked>Celibataire
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="mstatus" id="optionsRadios2" value="m">Marie(e)
								</label>
							</div>
						</div><!-- //form-group -->
						
						<div class="form-group">
							<label>Nationalite</label>
							<select class="form-control" name="nationality">
								@foreach ($countries as $country)
									<option value="{{ $country->alpha3Code }}">{{ $country->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label>Numero matricule</label>
							<input class="form-control" placeholder="Numero matricule" name="enrolment_id" type="text" value="" minlength="3" maxlength="50">
						</div>

						<div class="form-group">
							<label>Universite</label>
							<select class="form-control" name="school_id">
								@foreach ($schools as $school)
									<option value="{{ $school->id }}">{{ $school->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label>Phone</label>
							<input class="form-control" placeholder="Phone" name="phone" type="text" autofocus="" maxlength="50">
						</div>

						<!-- <div class="form-group">
							<input class="form-control" placeholder="GPhone" name="gphone" type="text" autofocus="" maxlength="50">
						</div> -->

						<div class="form-group">
							<label>Nom du pere</label>
							<input class="form-control" placeholder="Nom du pere" eng-name="Father's Name" name="father_name" type="text" autofocus="" maxlength="50">
						</div>

						<div class="form-group">
							<label>Uploader votre photo</label>
							<input type="file" name="id_url" class="form-control">
						</div>

						<div class="form-group">
							<label>Mot de passe</label>
							<input class="form-control" placeholder="mot de passe" eng-name="Password" id="passwordField" name="password" type="password" autofocus="" required maxlength="50" onkeyup='checkPasswords();'>
						</div>

						<div class="form-group">
							<label>Confirmez mot de passe</label>
							<input class="form-control" placeholder="Confirmez mot de passe" eng-name="password_confirmation" id="passwordConfirmationField" name="password_confirmation" type="password" autofocus="" maxlength="50" onkeyup='checkPasswords();'>
						</div>

						<div class="form-group">
							<label>Adresse</label>
							<textarea class="form-control" placeholder="Adresse" eng-name="Address" name="address"></textarea>
						</div>
						<input type="hidden" name="dor" value="<?php echo date('Y-m-d'); ?>">
						<button type="submit" href="" class="btn btn-primary">Creez utilisateur</button>
					</fieldset>
				</form>

			</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extraScripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

	function checkPasswords() {
		if (document.getElementById('passwordField').value ==
		    document.getElementById('passwordConfirmationField').value && document.getElementById('passwordField').value != '') {
		    document.getElementById('passwordConfirmationField').style.borderColor = 'green';
			document.getElementById('passwordField').style.borderColor = 'green';
		  	return true;
		}

	    document.getElementById('passwordConfirmationField').style.borderColor = 'red';
	  	document.getElementById('passwordField').style.borderColor = 'red';
  		return false;
	}

	$( function() {
	   $( ".datepicker" ).datepicker({
	    	'dateFormat': 'yy-mm-dd'
	    });
	} );
</script>
@endsection