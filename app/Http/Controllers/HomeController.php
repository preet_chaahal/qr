<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Student;
use App\School;
use App\User;
use Validator;

/* For generating QR Code */
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
/* /For QR Code */

use App\Http\Controllers\MiscController;
/* Intervention Image */
use Intervention\Image\ImageManagerStatic as Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');

        $new_students = Student::orderBy('id', 'desc')
                                    ->take(10)
                                    ->get();
        $students_count = Student::all()->count();
        $data = [
            'new_students'          =>  $new_students,
            'page'                  =>  'home',
            'student_count'         =>  $students_count,
            'new_student_count'     =>  count($new_students)
        ];
        return view('themes.lumino.pages.index', $data);
    }

    public function users_all(Request $request)
    {
        $this->middleware('auth');

        $students = Student::orderBy('id', 'desc')
                                    ->get();

        $data = [
            'students' => $students,
            'page'     => 'users-all',
        ];

        return view('themes.lumino.pages.users-all', $data);
    }

    public function profile(Request $request)
    {
        $this->middleware('auth');

        $user = Auth::user();

        $data = [
            'user' => $user,
            'page'     => 'profile',
        ];

        return view('themes.lumino.pages.profile', $data);
    }

    public function profile_save(Request $request)
    {
        $this->middleware('auth');

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|confirmed',
        ]);
        // echo $request->input('dob');
        // die();
        $status = 0;
        $message = "Something went wrong trying to update your profile";
        $password = Hash::make($request->password);
        
        $user = User::find(Auth::user()->id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = $password;

        if( $user->save() ){
            $data = [
                'status'    =>  1,
                'message'   =>  'Profile updated!'
            ];
        }
        // EBign a redirect request sesison flash doesnt retian the session data
        // $request->session()->flash('status', $data);
        return redirect()->route('home', $data);
    }

    public function users_add()
    {
        $this->middleware('auth');

        $countries = new MiscController;
        $countries = json_decode($countries->fetch_countries_data());

        $schools = School::where('removed', 0)
                                    ->get();

        $data = [
            'countries' => $countries,
            'schools'   => $schools,
            'page'      => 'users-all'
        ];
        return view('themes.lumino.pages.users-add', $data);
    }

    public function users_register()
    {
        $this->middleware('auth');

        $countries = new MiscController;
        $countries = json_decode($countries->fetch_countries_data());

        $schools = School::where('removed', 0)
                                    ->get();

        $data = [
            'countries' => $countries,
            'schools'   => $schools,
            'page'      => 'users-all'
        ];
        return view('themes.lumino.pages.users-register', $data);
    }

    public function users_save(Request $request)
    {
        $this->middleware('auth');

        $this->validate($request, [
            'id_url' => 'mimes:jpeg,jpg,png',
            'enrolment_id' => 'required|unique:students,enrolment_id|max:255'
        ]);
        // echo $request->input('dob');
        // die();
        $status = 0;
        $message = "Something went wrong trying to save the record";
        
        $form_data = $request->all();
        // print_r($form_data);
        $form_data['name'] = $request->fname ." ". $request->lname;
        $form_data['password'] = Hash::make($request->enrolment_id);

        $modified_image_name = '';
        if($request->hasFile('id_url')){
            $image = $request->file('id_url');
            $modified_image_name = 'up_'.time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('images/students'), $modified_image_name);
            
            //Resizing the image
            $path = public_path("images/students/".$modified_image_name);
            Image::make($path)->resize(168, 180)->save();
            
    	    // Saving to other location as well
    	    try{
                copy($path, '/var/www/html/images/uploads/students/'.$modified_image_name);
            } catch (\Exception $e) {
                // do nothing
            }
    	}

        $form_data['id_url'] = $modified_image_name;

        /* Require discussion */
        $form_data['added_by'] = '63';
        // $form_data['enrolment_id'] = 'KHDTEY213547HD';
        $form_data['qr_id'] = '';
        /* //Require discussion */

        // print_r($form_data);
        // die();

        if( Student::create($form_data) ){
            $data = [
                'status'    =>  1,
                'message'   =>  'Records saved!'
            ];
        }
        // EBign a redirect request sesison flash doesnt retian the session data
        // $request->session()->flash('status', $data);
        return redirect()->route('users-all', $data);
    }

    public function users($id=false)
    {
        $this->middleware('auth');

        $students = Student::orderBy('id', 'desc')
                                    ->get();
        $data = [
            'students' => $students,
            'page'     => 'users-all'
        ];
        return view('themes.lumino.pages.users-all', $data);
    }

    /*
     * Online reference for QR Code Generator package
     * --- https://github.com/endroid/QrCode
     */
    public function process_qr($id=false, $download = 1, $return_text=false)
    {
        $this->middleware('auth');

        if(empty($id))
            return false;
        
        $student = new Student;
        $qr_string = $student->get_qr_code($id);
        
        // Create a basic QR code
        $qrCode = new QrCode($qr_string);
        $qrCode->setSize(300);

        // Set advanced options
        $qrCode
            ->setWriterByName('png')
            ->setMargin(10)
            ->setEncoding('UTF-8')
            ->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH)
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0])
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255])
            // ->setLogoPath(__DIR__.'/../assets/')
            ->setSize(150)
            ->setValidateResult(false);

        if ($return_text)
            return $qrCode->writeString();

        // return $qrCode;
        if ($download == 0) {
            // Directly output the QR code
            header('Content-Type: '.$qrCode->getContentType());
            echo $qrCode->writeString();
        }
        else {
            // header('Content-Type: '.$qrCode->getContentType());
            $attachment_name = str_shuffle(time());
            $attachment_name .= ".png";

            header('Cache-Control: public');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=' . $attachment_name); 
            header('Content-Type: '. $qrCode->getContentType());
            header('Connection: Keep-Alive');
            
            echo $qrCode->writeString();
        }

        die();
    }

    /*
     * Online reference for QR Code Generator package
     * --- https://github.com/endroid/QrCode
     */
    public function generate_id_card($id='', $download = 1, $redirectRouteName=false)
    {
        // create an image manager instance with favored driver
        // $manager = new ImageManager(array('driver' => 'gd'));

        // to finally create image instances
        $img_src = public_path('images/id-card-design-beta-1.jpg');  
        $img_save_path = public_path('images/ID-Card-Design-Without-DP.jpg');

        //Calling to generate QR Code
        $student_visible_name = '';
        $qr_string = '';
        $profile_image = '';

        if ($id != ''){
            $user = new Student();
            $user = $user->find($id);
            $student_visible_name  =  ucfirst($user->fname) ." ". ucfirst($user->lname);
            $student_enrolment_id  =  $user->enrolment_id;

            $profile_image = public_path("images/students/no_user_thumb.png"); 
            if($user->id_url != '') {
        		$profile_image = public_path("images/students/".$user->id_url);
                if (!file_exists($profile_image))
        		    $profile_image = '/var/www/html/images/uploads/students/'.$user->id_url;
    	    }
    	}
    	// Resize image, incase of img uploaded from other panel,  is not resized upon insertion
    	$profile_image = Image::make($profile_image);
    	$profile_image->fit(168, 180);
    	$profile_image->save();
        $qr_image = Image::make($this->process_qr($user->id, 0, true));
        $image = Image::make($img_src)
                    ->insert($qr_image, 'bottom-right', 190, 105)
                    ->insert($profile_image, 'top-left', 41, 215)
                    //Inserting Name
                    ->text($student_visible_name, 40, 430, function($font) {
                        $font->file(public_path('fonts/SourceSansPro-Black.otf'));
                        $font->size(26);
                        $font->color('#3a3a3b');
                        $font->align('left');
                        $font->valign('top');
                    })
                    //Inserting ID
                    ->text((string) $student_enrolment_id, 580, 452, function($font) {
                        $font->file(public_path('fonts/SourceSansPro-Black.otf'));
                        $font->size(24);
                        $font->color('#fff');
                        $font->align('left');
                        $font->valign('top');
                    })
                    ->text((string) 'Powered by Panet', 580, 475, function($font) {
                        $font->file(public_path('fonts/SourceSansPro-Semibold.otf'));
                        $font->size(14);
                        $font->color('#fff');
                        $font->align('left');
                        $font->valign('top');
                    });

        if ($download == 0) {
            // Directly output the QR code
            header('Content-Type: image/png');
            
            echo $image->encode('png');
        }
        else {
            $attachment_name = str_shuffle(time());
            $attachment_name .= ".png";

            header('Cache-Control: public');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=' . $attachment_name); 
            header('Content-Type: image/png');
            header('Connection: Keep-Alive');
            echo $image->encode('png');
        }

        // header('Content-Type: image/png');
        // echo $img_save_path;
        die();
    }

}
