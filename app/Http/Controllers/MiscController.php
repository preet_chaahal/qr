<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

/* For generating QR Code */
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
/* /For QR Code */

// For accessing REST APIs of country data
use GuzzleHttp\Client;

/* Intervention Image */
use Intervention\Image\ImageManagerStatic as Image;

class MiscController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function fetch_countries_data()
    {
        $client = new Client();
        $res = $client->request('GET', 'https://restcountries.eu/rest/v2/all');

        // try{
        //     // file_put_contents(public_path('misc/countries.json'), $res->getBody());
        // }catch(\Exception $e){
        //     return false;
        // }
        return $res->getBody();
    }
    
    /*      DRAFT       */
    // public function fetch_countries()
    // {   
    //     // ini_set('memory_limit', '2G');
    //     $data = array();
    //     try{
    //         $data = file_get_contents(public_path('misc/countries.json'), $res->getBody());
    //     }catch(\Exception $e){
    //         print_r($e);
    //         echo "excetip";
    //         return false;
    //     }
    //     print_r($data);
    //     die();
    //     return json_decode($data);
    // }

}
