@extends('themes.lumino.layouts.master')

@section('extraStyles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb hidden">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">Icons</li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Acceuil</h1>
		</div>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-xs-12 col-md-6 col-lg-4">
			<div class="panel panel-teal panel-widget">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left">
						<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="large">{{ $new_student_count or 0 }}</div>
						<div class="text-muted">Nouveau utilisateurs</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-4">
			<div class="panel panel-teal panel-widget">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left">
						<svg class="glyph stroked male-users"><use xlink:href="#stroked-male-user"></use></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="large">{{ $student_count or 0 }}</div>
						<div class="text-muted">Nombre utilisateurs</div>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-blue panel-widget ">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left">
						<svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="large">120</div>
						<div class="text-muted">New Orders</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-red panel-widget">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left">
						<svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"></use></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="large">25.2k</div>
						<div class="text-muted">Page Views</div>
					</div>
				</div>
			</div>
		</div> -->
	</div><!--/.row-->
							
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				
				<div class="panel-heading">Derniers ajouts</div>
				<div class="panel-body">
					<table id="userDataTbl" class="table table-striped table-bordered" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th></th>
				                <th>Nom utilisateur</th>
				                <th>Prenom</th>
				                <th>Nom de famille</th>
				                <th>Date de Nais</th>
				                <th>Phone</th>
				                <th>Adresse</th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				            	<th>Image</th>
				                <th>Nom utilisateur</th>
				                <th>Prenom</th>
				                <th>Nom de famille</th>
				                <th>Date de Nais</th>
				                <th>Phone</th>
				                <th>Adresse</th>
				            </tr>
				        </tfoot>
				        <tbody>
				            @foreach ($new_students as $new_student)
					            <tr>
					            	<td><img style="height: 100px; width: 90px;" 
					            			@if($new_student->id_url != '')
					            				src="{{ url('images/students') }}/{{ $new_student->id_url }}"
					            			@else
					            				src="{{ url('images/students') }}/no_user_thumb.png"
					            			@endif
					            		>
					            	</td>
					                <td>{{ $new_student->username }}</td>
					                <td>{{ $new_student->fname or 'N.A.' }}</td>
					                <td>{{ $new_student->lname or 'N.A.' }}</td>
					                <td><?php $dob = \DateTime::createFromFormat('Y-m-d', $new_student->dob); echo $dob->format('d/m/Y'); ?></td>
					                <td>{{ $new_student->phone or 'N.A.' }}</td>
					                <td>{{ $new_student->address or 'N.A.' }}</td>
					            </tr>
				            @endforeach
				        </tbody>
				    </table>
				</div><!-- /.panel-body -->
			
			</div>
		</div><!--/.col-->
	</div><!--/.row-->
</div>	<!--/.main-->
@endsection

@section('extraScripts')
<!-- Data Table -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">	
	$(document).ready(function() {
	    $('#userDataTbl').DataTable({
	    	"language": {
	            "lengthMenu": "Lister _MENU_ donnees",
	            "zeroRecords": "Nothing found - sorry",
	            "info": "Lister page _PAGE_ of _PAGES_",
	            "infoEmpty": "No records available",
	            "infoFiltered": "(filtered from _MAX_ total records)",
	        	"search": "Chercher:"
	        }	
    	});
	} );
</script>
@endsection