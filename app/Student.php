<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    protected $table = 'students';

    public $timestamps = false;

    protected $fillable = ['fname', 'lname', 'username', 'password', 'dob', 'pob', 'sex', 'mstatus', 'nationality', 'phone', 'gphone', 'father_name', 'address', 'dor', 'name', 'added_by', 'enrolment_id', 'school_id', 'id_url', 'qr_id'];

    /*
     * Generating the QR Code by using the unique id and 4 letters of the username
     */
    public function get_qr_code($id)
    {
    	$data = DB::table('students')
    				->where('id', $id)
    				->first();
    	
    	if(empty($data))
    		return false;
    	
    	// return "{$data['id']}_". (string) substr($data['username'], 0, 4);
    	return $data->enrolment_id;
    }
}
