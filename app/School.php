<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class School extends Model
{
    public $timestamps = false;

    protected $fillable = ['code', 'name', 'address', 'logo_url'];

}
