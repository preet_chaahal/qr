@extends('themes.lumino.layouts.master')

@section('extraStyles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb hidden">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">Icons</li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Profile</h3>
		</div>
	</div><!--/.row-->
							
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default">
				
				<!-- <div class="panel-heading">All users</div> -->
				<div class="panel-body">
					

					<div class="login-panel panel panel-default">
						<!-- <div class="panel-heading">Add</div> -->
						<div class="panel-body">
							<form role="form" method="post" action="{{ route('profile-save') }}" enctype="multipart/form-data">
								<fieldset>
									{{ csrf_field() }}
									<div class="form-group">
										<input class="form-control" placeholder="nom" eng-name="Name" name="name" type="text" minlength="3" maxlength="50" value="{{ $user->name }}">
									</div>

									<div class="form-group">
										<input class="form-control" placeholder="Email" eng-name="Email" name="email" value="{{ $user->email }}" type="text" value="" minlength="3" maxlength="50">
									</div>

									<div class="form-group">
										<input class="form-control" placeholder="Mot de passe" eng-name="Email" name="password" type="password" value="" minlength="3" maxlength="50">
									</div>

									<div class="form-group">
										<input class="form-control" placeholder="Mot de passe" eng-name="Confirmez Mpass" name="password_confirmation" type="password" value="" minlength="3" maxlength="50">
									</div>
									
									<button type="submit" href="" class="btn btn-primary">Creez utilisateur</a>
								</fieldset>
							</form>
						</div>
					</div>
					
				</div><!-- /.panel-body -->
			
			</div>
		</div><!--/.col-->
	</div><!--/.row-->
</div>	<!--/.main-->
@endsection