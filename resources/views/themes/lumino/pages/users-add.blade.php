@extends('themes.lumino.layouts.master')

@section('extraStyles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb hidden">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">Icons</li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Ajouter utilisateur</h3>
		</div>
	</div><!--/.row-->
							
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default">
				
				<!-- <div class="panel-heading">All users</div> -->
				<div class="panel-body">
					

					<div class="login-panel panel panel-default">
						<!-- <div class="panel-heading">Add</div> -->
						<div class="panel-body">
							<form role="form" method="post" action="{{ route('users-save') }}" enctype="multipart/form-data">
								<fieldset>
									{{ csrf_field() }}
									<div class="form-group">
										<label>Prenom</label>
										<input required class="form-control" placeholder="Prenom" eng="First name" name="fname" type="text" autofocus="" maxlength="50">
									</div>
									
									<div class="form-group">
										<label>Nom de famille</label>
										<input required class="form-control" placeholder="Nom de famille" eng="Last name" name="lname" type="text" autofocus="" maxlength="50">
									</div>

									<div class="form-group">
										<label>Email</label>
										<input required class="form-control" placeholder="Email" eng-name="Email" name="email" type="email" value="" minlength="3" maxlength="255">
									</div>
									
									<div class="form-group">
										<label>Nom utilisateur</label>
										<input required class="form-control" placeholder="Nom utilisateur" eng-name="Username" name="username" type="text" value="" minlength="3" maxlength="50">
									</div>

									<div class="form-group">
										<label>Date de Nais</label>
										<input required class="form-control datepicker" placeholder="DOB" name="dob" type="text" value="">
									</div>
									
									<!-- <div class="form-group">
										<input class="form-control" placeholder="POB" name="pob" type="text" value="">
									</div> -->
									
									<div class="form-group">
										<label>Sexe</label>
										<div class="radio">
											<label>
												<input required type="radio" name="sex" id="optionsRadios1" value="m" checked>Homme
											</label>
										</div>
										<div class="radio">
											<label>
												<input required type="radio" name="sex" id="optionsRadios2" value="f">Femme
											</label>
										</div>
									</div><!-- //form-group -->

									<div class="form-group">
										<label>Statut Marital</label>
										<div class="radio">
											<label>
												<input required type="radio" name="mstatus" id="optionsRadios1" value="s" checked>Celibataire
											</label>
										</div>
										<div class="radio">
											<label>
												<input required type="radio" name="mstatus" id="optionsRadios2" value="m">Marie(e)
											</label>
										</div>
									</div><!-- //form-group -->
									
									<div class="form-group">
										<label>Nationality</label>
										<select class="form-control" name="nationality">
											@foreach ($countries as $country)
												<option value="{{ $country->alpha3Code }}">{{ $country->name }}</option>
											@endforeach
										</select>
									</div>

									<div class="form-group">
										<input required class="form-control" placeholder="Enrollment ID" name="enrolment_id" type="text" value="" minlength="3" maxlength="50">
									</div>

									<div class="form-group">
										<label>Universite</label>
										<select class="form-control" name="school_id">
											@foreach ($schools as $school)
												<option value="{{ $school->id }}">{{ $school->name }}</option>
											@endforeach
										</select>
									</div>

									<div class="form-group">
										<label>Phone</label>
										<input class="form-control" placeholder="Phone" name="phone" type="text" autofocus="" maxlength="50">
									</div>

									<!-- <div class="form-group">
										<input class="form-control" placeholder="GPhone" name="gphone" type="text" autofocus="" maxlength="50">
									</div> -->

									<div class="form-group">
										<label>Nom du pere</label>
										<input class="form-control" placeholder="Nom du pere" eng-name="Father's Name" name="father_name" type="text" autofocus="" maxlength="50">
									</div>

									<div class="form-group">
										<label>Uploader votre photo</label>
										<input type="file" name="id_url" class="form-control">
									</div>

									<div class="form-group">
										<textarea class="form-control" placeholder="Adresse" eng-name="Address" name="address"></textarea>
									</div>
									<input type="hidden" name="dor" value="<?php echo date('Y-m-d'); ?>">
									<button type="submit" href="" class="btn btn-primary">Creez utilisateur</a>
								</fieldset>
							</form>
						</div>
					</div>
					
				</div><!-- /.panel-body -->
			
			</div>
		</div><!--/.col-->
	</div><!--/.row-->
</div>	<!--/.main-->
@endsection

@section('extraScripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( ".datepicker" ).datepicker({
    	'dateFormat': 'yy-mm-dd'
    });
  } );
  // $( ".datepicker" ).on( "change", function() {
  //     $( "#datepicker" ).datepicker( "option", "dateFormat", $( this ).val() );
  //   });
  </script>
@endsection